# Legal Documents for Codeberg

This repository contains the legal documents for the Codeberg e.V. (German association). The legally binding documents are at the root of this repo in their original language. These are:

- [Imprint.md](Imprint.md) (En): Legally required information about the platform operator (Codeberg)
- [PrivacyPolicy.md](PrivacyPolicy.md) (En): As title - the privacy policy.
- [Satzung.md](Satzung.md) (De): The bylaws text. There is an unofficial English translation available.
- [TermsOfUse.md](TermsOfUse.md) (En): The terms of use for the platform.

The translations go into their corresponding folder. Please note: The translations are unofficial and there is no guarantee for correctness and actuality.

## Translation help

We are always looking for help in translating these documents. Feel free to open a Merge Request adding a language you speak, updating a translation after the original file changed or correcting stuff.

